import { DecimalPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { RouterLink } from '@angular/router';
import { ConditionsAndZip } from 'app/conditions-and-zip.type';

@Component({
  selector: 'app-current-condition-item',
  standalone: true,
  imports: [RouterLink, DecimalPipe],
  templateUrl: './current-condition-item.component.html',
  styleUrl: './current-condition-item.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CurrentConditionItemComponent {
  @Input({ required: true }) condition: ConditionsAndZip;
  @Input({ required: true }) icon: string;
  @Input({ required: true }) iconAlt: string;
}
