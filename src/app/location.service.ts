import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export const LOCATIONS: string = 'locations';

@Injectable()
export class LocationService {
  private locationAddedSource = new Subject<string>();
  private locationRemovedSource = new Subject<string>();

  private _locations: string[] = [];

  locationAdded$ = this.locationAddedSource.asObservable();
  locationRemoved$ = this.locationRemovedSource.asObservable();

  constructor() {
    const locString = localStorage.getItem(LOCATIONS);
    this._locations = locString ? JSON.parse(locString) : [];
  }

  get locations(): string[] {
    return [...this._locations];
  }

  addLocation(zipcode: string) {
    if (this.locations.includes(zipcode)) {
      return;
    }

    this._locations = [...this.locations, zipcode];
    localStorage.setItem(LOCATIONS, JSON.stringify(this.locations));
    this.locationAddedSource.next(zipcode);
  }

  removeLocation(zipcode: string) {
    const indexToRemove = this.locations.indexOf(zipcode);

    if (indexToRemove > -1) {
      this.locations.splice(indexToRemove, 1);
      localStorage.setItem(LOCATIONS, JSON.stringify(this.locations));
      this.locationRemovedSource.next(zipcode);
    }
  }
}
