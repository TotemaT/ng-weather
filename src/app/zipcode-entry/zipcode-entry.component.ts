import { ChangeDetectionStrategy, Component } from '@angular/core';
import { LocationService } from '../location.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-zipcode-entry',
  templateUrl: './zipcode-entry.component.html',
  standalone: true,
  imports: [FormsModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ZipcodeEntryComponent {
  constructor(private service: LocationService) {}

  zipcode: string;

  addLocation() {
    if (!this.zipcode) {
      return;
    }

    this.service.addLocation(this.zipcode);
    this.zipcode = '';
  }
}
