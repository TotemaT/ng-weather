import { Component } from '@angular/core';
import { WeatherService } from '../weather.service';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { Forecast } from './forecast.type';
import { DecimalPipe, DatePipe, AsyncPipe } from '@angular/common';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-forecasts-list',
  templateUrl: './forecasts-list.component.html',
  styleUrls: ['./forecasts-list.component.css'],
  standalone: true,
  imports: [RouterLink, DecimalPipe, DatePipe, AsyncPipe],
})
export class ForecastsListComponent {
  zipcode: string;
  forecast$: Observable<Forecast>;

  constructor(protected weatherService: WeatherService, route: ActivatedRoute) {
    this.forecast$ = route.params.pipe(
      map((params) => {
        this.zipcode = params.zipcode;
        return this.zipcode;
      }),
      switchMap((zipcode: string) => weatherService.getForecast(zipcode)),
    );
  }
}
