import { HttpResponse } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { REQUEST_CACHE_TIMEOUT_MINUTES } from 'main';

type Data = {
  cachedAt: number;
  data: HttpResponse<unknown>;
  url: string;
};

@Injectable({
  providedIn: 'root',
})
export class CacheService {
  private static readonly KEY = 'requestCache';

  private cacheDelay = inject(REQUEST_CACHE_TIMEOUT_MINUTES);

  get cache(): Data[] {
    const cache = localStorage.getItem(CacheService.KEY);

    return cache ? JSON.parse(cache) : [];
  }

  set(data: Data) {
    this.save([...this.cache, data]);
  }

  get(url: string): HttpResponse<unknown> | undefined {
    const cache = this.cache;
    const cachedData = cache.find((data) => data.url === url);

    if (!cachedData) {
      return undefined;
    }

    if (!this.isValid(cachedData)) {
      cache.splice(cache.indexOf(cachedData), 1);
      this.save(cache);

      return undefined;
    }

    return cachedData.data;
  }

  private isValid(data: Data): boolean {
    return Date.now() - data.cachedAt <= this.cacheDelay * 1000;
  }

  private save(cache: Data[]) {
    localStorage.setItem(CacheService.KEY, JSON.stringify(cache));
  }
}
