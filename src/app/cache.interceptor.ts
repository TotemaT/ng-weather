import { HttpResponse, type HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { CacheService } from './cache.service';

export const cacheInterceptor: HttpInterceptorFn = (req, next) => {
  if (req.method !== 'GET') {
    return next(req);
  }

  const cache = inject(CacheService);

  const cachedData = cache.get(req.urlWithParams);

  if (cachedData) {
    return of(
      new HttpResponse({
        ...cachedData,
      }),
    );
  }

  return next(req).pipe(
    tap((response) => {
      if (response instanceof HttpResponse) {
        cache.set({
          cachedAt: Date.now(),
          data: response,
          url: req.urlWithParams,
        });
      }
    }),
  );
};
