import { AfterContentInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChildren, OnDestroy, QueryList, inject } from '@angular/core';
import { TabComponent } from './tab/tab.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-tabs',
  standalone: true,
  imports: [TabComponent],
  templateUrl: './tabs.component.html',
  styleUrl: './tabs.component.css',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TabsComponent implements AfterContentInit, OnDestroy {
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  private cdRef = inject(ChangeDetectorRef);
  private unsubscribe$ = new Subject<void>();

  ngAfterContentInit() {
    this.tabs.changes.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
      this.setActiveTab();
      this.cdRef.detectChanges();
    });

    this.setActiveTab();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  selectTab(tab: TabComponent) {
    this.tabs.toArray().forEach((tab) => tab.active.set(false));
    tab.active.set(true);
  }

  private setActiveTab() {
    if (!this.tabs.find((tab) => tab.active())) {
      this.selectTab(this.tabs.first);
    }
  }
}
