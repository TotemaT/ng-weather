import { InjectionToken, enableProdMode, importProvidersFrom } from '@angular/core';

import { environment } from './environments/environment';
import { AppComponent } from './app/app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { routing } from './app/app.routing';
import { RouterModule } from '@angular/router';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule, bootstrapApplication } from '@angular/platform-browser';
import { WeatherService } from './app/weather.service';
import { LocationService } from './app/location.service';
import { cacheInterceptor } from 'app/cache.interceptor';

export const REQUEST_CACHE_TIMEOUT_MINUTES = new InjectionToken<number>('requestCacheTimeoutMinutes');

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(
      BrowserModule,
      FormsModule,
      RouterModule,
      routing,
      ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    ),
    LocationService,
    WeatherService,
    { provide: REQUEST_CACHE_TIMEOUT_MINUTES, useValue: 120 },
    provideHttpClient(withInterceptors([cacheInterceptor])),
  ],
});
